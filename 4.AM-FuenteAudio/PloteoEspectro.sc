//Luego de ejecutar Sim1-InicVar y Sim1-AM.zcos, habiendo obtenido "fmod" (señal modulada) se analizan las tres señales involucradas en el proceso, en el dominio de la frecuencia.
ffm=fft(fm.values); ffc=fft(fc.values); ffmod=fft(fmod.values); ffm2=fft(fm2.values);
figure(1);
subplot(3, 1, 1); plot(abs(ffm)); xlabel('w'); ylabel('Modulante'); title ('EN EL DOMINIO DE LA FRECUENCIA');
subplot(3, 1, 2); plot(abs(ffc)); xlabel('w'); ylabel('Portadora');
subplot(3, 1, 3); plot(abs(ffmod)); xlabel('w'); ylabel('Modulada');
//
figure(3);
subplot(3, 1, 1); plot(abs(ffmod)); xlabel('w'); ylabel('Modulada'); title ('EN EL DOMINIO DE LA FRECUENCIA');
subplot(3, 1, 2); plot(abs(ffc)); xlabel('w'); ylabel('Portadora');
subplot(3, 1, 3); plot(abs(ffm2)); xlabel('w'); ylabel('Modulante');
