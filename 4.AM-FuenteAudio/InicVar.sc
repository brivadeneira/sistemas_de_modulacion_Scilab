[fm.values, fs, nbits]=wavread(input('Nombre del archivo de audio (entre comillas): '));
tam=length(fm.values);
fc.time=[0:1/fs:(tam-1)/fs]';
wm=10*%pi*fs;
wc=5000*%pi;
fc.values=cos(wc.*fc.time);
sound(fm.values);
//El bloque "FromWorkspace" requiere que la var sea un vector columna
fm.time=fc.time;
fm.values=fm.values';
m=input('Índice de modulación (m): ');
Em=max(fm.values);
Ec=Em/m;
//FILTRO!
//hb = analpf(9, 'butt', [0 0], wm);
hb = analpf(7, 'butt', [0 0], 2*wm);
num = coeff(hb(2));
den = coeff(hb(3));
num = num(length(num):-1:1);
den = den(length(den):-1:1);
// FILTRO: (RC paralelo), para eliminar Acos, luego de rectificar
// recurriendo a la consideración de que 1/fc < RC < 1/fm (media geométrica)
RC=sqrt(1/(wc/2*%pi)*1/(wm/2*%pi))*50;
R=1.7;
// Función de detección de envolvente para el bloque
//fn='(u(2)<u(1))*u(1)*exp(-RC)+(u(2)>=u(1))*u(2)';
//num = coeff(hb(2));
//den = coeff(hb(3));
//num = num(length(num):-1:1);
//den = den(length(den):-1:1);
///

