//Moldulantes
fm1.time=[0:0.001:2]';
fm2.time=fm1.time;
fm3.time=fm1.time;
wm1=4*%pi; wm2=10*%pi; wm3=30*%pi;
fm1.values=cos(wm1.*fm1.time);
fm2.values=cos(wm2.*fm2.time);
fm3.values=cos(wm3.*fm3.time);
//Portadoras
wc1=10*wm1; wc2=10*wm2; wc3=10*wm3;
fc1.time=fm1.time;
fc2.time=fm1.time;
fc3.time=fm1.time;
fc1.values=cos(wc1.*fc1.time);
fc2.values=cos(wc2.*fc2.time);
fc3.values=cos(wc3.*fc3.time);
tam=length(fm1.time);
//
fmod1.time=fm1.time;
fmod2.time=fm1.time;
fmod3.time=fm1.time;
fmod1.values=fm1.values.*fc1.values;
fmod2.values=fm2.values.*fc2.values;
fmod3.values=fm3.values.*fc3.values;
//
fmux.time=fm1.time;
fmux.values=fmod1.values+fmod2.values+fmod3.values;
// FILTRO: (analógico), método Butt, orden 7
hb = analpf(7, 'butt', [0 0], (wm1+wc1));
num = coeff(hb(2));
den = coeff(hb(3));
num = num(length(num):-1:1);
den = den(length(den):-1:1);
// FILTRO: (analógico), método Butt, orden 7
hb2 = analpf(7, 'butt', [0 0], (2*wc1));
num2 = coeff(hb2(2));
den2 = coeff(hb2(3));
num2 = num2(length(num2):-1:1);
den2 = den2(length(den2):-1:1);
//VCO
kc=0.5;
fc=50;
// FILTRO: (analógico), método Butt, orden 7
hb3 = analpf(7, 'butt', [0 0], (wc1+wm1));
num3 = coeff(hb3(2));
den3 = coeff(hb3(3));
num3 = num2(length(num3):-1:1);
den3 = den2(length(den3):-1:1);
