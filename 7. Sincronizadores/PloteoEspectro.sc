figure(300);
subplot(4, 1, 1); plot(fftshift(abs(fft(fmod1.values))));
title('Multiplexación'); xlabel('AM1 Modulada');
subplot(4, 1, 2); plot(fftshift(abs(fft(fmod2.values))));
xlabel('AM2 Modulada');
subplot(4, 1, 3); plot(fftshift(abs(fft(fmod3.values))));
xlabel('AM3 Modulada');
subplot(4, 1, 4); plot(fftshift(abs(fft(fmux.values))));
xlabel('Multiplexada');
//
figure(301);
subplot(4, 1, 1); plot(fftshift(abs(fft(fmux.values))));
title('Demodulación'); xlabel('Multiplexada');
subplot(4, 1, 2); plot(fftshift(abs(fft(fmfpb1.values))));
xlabel('AM1 Modulada');
subplot(4, 1, 3); plot(fftshift(abs(fft(fmcuad.values))));
xlabel('Salida Elevador al cuadrado');
subplot(4, 1, 4); plot(fftshift(abs(fft(fmfpb2.values))));
xlabel('Filtro pasa Bandas 2fc');
//
figure(302);
subplot(4, 1, 1); plot(fftshift(abs(fft(vco.values))));
title('Demodulación'); xlabel('Salida del VCO');
subplot(4, 1, 2); plot(fftshift(abs(fft(vcox2.values))));
xlabel('VCO x2f');
subplot(4, 1, 3); plot(fftshift(abs(fft(vcoxfmod.values))));
xlabel('VCO x FMOD');
subplot(4, 1, 4); plot(fftshift(abs(fft(fm.values))));
xlabel('fm');
