//Se inicializan las variables que almacenan las frecuencias portadora y modulante, así como la amplitud de la modulante. (Ejecutar Sim1.4-AM.zcos al finalizar)
wm=input('Frecuencia de la señal modulante (Wm): ');
Em=input('Valor pico de la señal modulante (Em): ');
wc=input('Frecuencia de la señal portadora (Wc): ');
// Plot a zero mean gaussian white noise with the variance 1.
// To use a different variance, multiply rand() by the square root of the variance.
sig = 1; // Standard deviation of the white gaussian noise
fr.time=[0.001:0.001:2]; fr.values = rand(1, 2000, "normal");
fr.time=fr.time'; fr.values=fr.values';
// FILTRO: (analógico), método Butt, orden 7
hb = analpf(7, 'butt', [0 0], wm);
num = coeff(hb(2));
den = coeff(hb(3));
num = num(length(num):-1:1);
den = den(length(den):-1:1);
