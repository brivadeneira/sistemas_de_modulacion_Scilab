//Se inicializan las variables que almacenan las señales modulante y portadora. (Ejecutar AMPS-FuenteAudio.zcos en XCOS al finalizar)
[fm.values, fs, nbits]=wavread(input('Nombre del archivo de audio (entre comillas): ')); //Modulante
tam=length(fm.values); //tamaño del vector fm.values
fc.time=[0:1/fs:(tam-1)/fs]'; //vector tiempo para la portadora (vector columna)
wm=100*fs; wc=1000*fs; //se estima wm, se determina una wc que cumpla con T de muestreo.
fc.values=cos(wc.*fc.time); //vector portadora
sound(fm.values);//se reproduce el vector modulante
fin=(1/fs)*length(fc.time); //var que limita el tiempo de simulación (bloque "END")
//El bloque "FromWorkspace" requiere que la var sea un vector columna
fm.time=fc.time; //vector de tiempos para la modulante
fm.values=fm.values'; //vector modulante -> vector columna
//FILTRO PASA BAJAS: (analógico), método Butt, orden 7. Ver superbloque dentro del receptor.
hb = analpf(7, 'butt', [0 0], wc/1000);
num = coeff(hb(2));
den = coeff(hb(3));
num = num(length(num):-1:1);
den = den(length(den):-1:1);
