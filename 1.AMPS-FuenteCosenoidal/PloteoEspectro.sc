//Luego de ejecutar InicVar.sc y AMPS-FuenteCosenoidal.zcos, habiendo obtenido "fmod" (señal modulada) se analizan las tres señales involucradas en el proceso, en el dominio de la frecuencia.
clf(); //Limpia las ventanas de gráficos.
//Las ventanas de gráficos número 0 y 2 corresponden a las gráficas en tiempo.
//Transmisor
ffm=fft(fm.values, -1); //Modulante 
ffc=fft(fc.values, -1); //Portadora
ffmod=fft(fmod.values, -1); //Modulada
ffm2=fft(fm2.values, -1); //Modulante recuperada
figure(1);
subplot(3, 1, 1); plot2d(fftshift(abs(ffm))); xlabel('w'); ylabel('Modulante'); 
title ('TRANSMISOR');
subplot(3, 1, 2); plot2d(fftshift((abs(ffc)))); xlabel('w'); ylabel('Portadora');
subplot(3, 1, 3); plot2d(fftshift((abs(ffmod)))); xlabel('w'); ylabel('Modulada');
//Receptor
figure(3);
subplot(3, 1, 1); plot2d(fftshift((abs(ffmod)))); xlabel('w'); ylabel('Modulada'); 
title ('RECEPTOR');
subplot(3, 1, 2); plot2d(fftshift((abs(ffc)))); xlabel('w'); ylabel('Portadora');
subplot(3, 1, 3); plot2d(fftshift((abs(ffm2)))); xlabel('w'); ylabel('Modulante');
