//Se inicializan las variables que almacenan las frecuencias portadora y modulante, así como la amplitud de la modulante. (Ejecutar Sim1.4-AM.zcos al finalizar)
wm=input('Frecuencia de la señal modulante (Wm): ');
Em=input('Valor pico de la señal modulante (Em): ');
//fm.time=[0:0.01:(10*2*%pi/wm)]';
//fm.values=Ec*cos(wm.*fm.time);
wc=input('Frecuencia de la señal portadora (Wc): ');
//fc.time=fm.time;
//fc.values=cos(wc.*fc.time);
m=input('Índice de modulación (m): ');
Ec=Em/m;
//buffer=
// FILTRO: (analógico), método Butt, orden 7
hb = analpf(7, 'butt', [0 0], 2*wm);
num = coeff(hb(2));
den = coeff(hb(3));
num = num(length(num):-1:1);
den = den(length(den):-1:1);
// FILTRO: (RC paralelo), para eliminar Acos, luego de rectificar
// recurriendo a la consideración de que 1/fc < RC < 1/fm (media geométrica)
RC=sqrt(1/(wc/2*%pi)*1/(wm/2*%pi))*50;
R=1.7;
// Función de detección de envolvente para el bloque
//fn='(u(2)<u(1))*u(1)*exp(-RC)+(u(2)>=u(1))*u(2)';
//num = coeff(hb(2));
//den = coeff(hb(3));
//num = num(length(num):-1:1);
//den = den(length(den):-1:1);
///
//Var de simulación:
per=0.0001;
y=3; ref=0.5; buff=50;
// FILTRO: (analógico), método Butt, orden 7
hb = analpf(7, 'butt', [0 0], wc/1000);
num = coeff(hb(2));
den = coeff(hb(3));
num = num(length(num):-1:1);
den = den(length(den):-1:1);
// FILTRO: Pasa altas (surge de transformar hb)
hp=trans(hb,'hp', wc/1000);
num2 = coeff(hb(2));
den2 = coeff(hb(3));
num2 = num2(length(num):-1:1);
den2 = den2(length(den):-1:1);
