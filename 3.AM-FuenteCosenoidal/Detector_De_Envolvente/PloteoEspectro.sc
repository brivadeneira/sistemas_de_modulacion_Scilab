//Luego de ejecutar InicVar.sc y AM-FuenteCosenoidal.zcos, habiendo obtenido "fmod" (señal modulada) se analizan las tres señales involucradas en el proceso, en el dominio de la frecuencia.
clf(); //Limpia las ventanas de gráficos.
//Las ventanas de gráficos número 0 y 2 corresponden a las gráficas en tiempo.
//Transmisor
ffm=mtlb_fft(fm.values); //Modulante 
ffc=mtlb_fft(fc.values); //Portadora
ffmod=mtlb_fft(fmod.values); //Modulada
ffm2=mtlb_fft(fm2.values); //Modulante recuperada
figure(1);
subplot(3, 1, 1); plot2d(fftshift(abs(ffm))); xlabel('w'); ylabel('Modulante'); 
title ('EN EL DOMINIO DE LA FRECUENCIA');
subplot(3, 1, 2); plot2d(fftshift(abs(ffc))); xlabel('w'); ylabel('Portadora');
subplot(3, 1, 3); plot2d(fftshift(abs(ffmod))); xlabel('w'); ylabel('Modulada');
//
figure(3);
subplot(3, 1, 1); plot2d(fftshift(abs(ffmod))); xlabel('w'); ylabel('Modulada'); title ('EN EL DOMINIO DE LA FRECUENCIA');
subplot(3, 1, 2); plot2d(fftshift(abs(ffc))); xlabel('w'); ylabel('Portadora');
subplot(3, 1, 3); plot2d(fftshift(abs(ffm2))); xlabel('w'); ylabel('Modulante');

