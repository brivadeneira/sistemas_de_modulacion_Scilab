//Se inicializan las variables que almacenan las frecuencias portadora y modulante, así como la amplitud de la modulante. (Ejecutar ErroresFaseFrec.zcos al finalizar)
ew=input('Error de frecuencia: ');
phi=input('Error de fase: ');
// FILTRO: (analógico), método Butt, orden 7
hb = analpf(7, 'butt', [0 0], wm);
num = coeff(hb(2));
den = coeff(hb(3));
num = num(length(num):-1:1);
den = den(length(den):-1:1);
